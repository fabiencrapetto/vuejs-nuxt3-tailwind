// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    modules: [
        '@nuxtjs/tailwindcss',
        '@nuxtjs/google-fonts',
    ],
    postcss: {
        plugins: {
            'postcss-import': {},
            'tailwindcss/nesting': {},
            tailwindcss: {},
            autoprefixer: {},
        },
    },
    css: [
        '@/assets/css/main.css'
    ],
    tailwindcss: {
        exposeConfig: false,
        viewer: true,
    },
    googleFonts: {
        families: {
            Montserrat: true,
            'Bebas+Neue': true,
        },
        display: 'swap',
        prefetch: true
    }
})