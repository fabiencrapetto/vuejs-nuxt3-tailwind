import defaultColors from "tailwindcss/colors";

module.exports = {
  darkMode: "class",
  theme: {
    extend: {
      textColor: {
        skin: {
          base: "var(--color-text--base)",
          muted: "var(--color-text--muted)",
          inverted: "var(--color-text--inverted)",
          accent: "var(--color-accent)",
          "btn-accent--txt": "var(--color-accent--inverted)",
          "btn-inverted--txt": "var(--color-invert--inverted)",
        },
      },
      borderColor: {
        skin: {
          accent: "var(--color-accent)",
        },
      },
      backgroundColor: {
        skin: {
          back: "var(--color-background)",
          fill: "var(--color-fill)",
          "btn-accent": "var(--color-accent)",
          "btn-accent--hover": "var(--color-accent--darker)",
          "btn-inverted": "var(--color-invert)",
          "btn-inverted--hover": "var(--color-invert--darker)",
        },
      },
      gradientColorStops: {
        skin: {
          hue: "var(--color-background)",
        },
      },
      height: {
        "screen-25": "25vh",
        "screen-50": "50vh",
        "screen-75": "75vh",
      },
      maxHeight: (theme) => theme("height"),
      minHeight: (theme) => theme("height"),
    },
    colors: {
      transparent: defaultColors.transparent,
      valid: defaultColors.emerald,
      invalid: defaultColors.rose,
      // accent: defaultColors.fuchsia,
    },
    fontFamily: {
      sans: ["Montserrat"],
      header: ["Bebas+Neue"],
    },
  },
  plugins: [],
  content: [
    `~/components/**/*.{vue,js,ts}`,
    `~/layouts/**/*.vue`,
    `~/pages/**/*.vue`,
    `~/composables/**/*.{js,ts}`,
    `~/plugins/**/*.{js,ts}`,
    `~/utils/**/*.{js,ts}`,
    `~/App.{js,ts,vue}`,
    `~/app.{js,ts,vue}`,
    `~/Error.{js,ts,vue}`,
    `~/error.{js,ts,vue}`,
    `~/app.config.{js,ts}`,
    `!~/**/*.{spec,test}.*`, // mind the ! at the start
  ],
};
